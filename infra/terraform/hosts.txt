[servers]
vm-1 ansible_host=158.160.71.122
[monitoring]
vm-3 ansible_host=158.160.76.112
[gitlab-runner]
vm-2 ansible_host=158.160.74.167
[grafana]
vm-4 ansible_host=158.160.76.30
